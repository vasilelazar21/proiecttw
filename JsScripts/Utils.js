var ERROR_TIMEOUT = 4000; //ms

class Logger{
	constructor(){
		
	}

	static init(){
		this.startTimeStamp = new Date().getTime();
	}

	static log(data){
	    console.log("[" + (new Date().getTime() - this.startTimeStamp)  + "]" + "::" + data);
	}
}

var errorElement;

window.onload = function(){
	Logger.init();
	errorElement = document.getElementById("error_message");

	if(lastPhpError.length > 0){
		raiseError(lastPhpError);
	}

	Logger.log("Page initialised");
}

/*function hashInputPassword(input){
	input.value = input.value; //todo
}*/

function submitLogin(form){
	Logger.log("submitLogin");

	if(form.elements["email"].value.length == 0){
		raiseError("Insert email");
		return;
	}

	if(form.elements["password"].value.length == 0){
		raiseError("Insert password");
		return;
	}

	//hashInputPassword(form.elements["password"]);
	form.submit();
}

function submitRegister(form){
	Logger.log("submitRegister");

	if(form.elements["email"].value.length == 0){
		raiseError("Insert email");
		return;
	}

	if(form.elements["password"].value.length == 0){
		raiseError("Insert password");
		return;
	}

	if(form.elements["password"].value != form.elements["confirm_password"].value){
		raiseError("Password and confirmation don't match!");
		return;
	}

	if(form.elements["password"].value.length < 6){
		raiseError("Password must be between 6 and 24 characters!");
		return;
	}

	//hashInputPassword(form.elements["password"]);
	//hashInputPassword(form.elements["confirm_password"]);
	form.submit();
}

function submitLogout(){
	document.location.href = "logout.php";
}

function raiseError(errorMessage){
	errorElement.innerHTML = errorMessage;
	errorElement.classList.remove("hidden");
	setTimeout(dropError,  ERROR_TIMEOUT);
}

function dropError(){
	errorElement.classList.add("hidden");
}