function submitCreateArchive(form){
	Logger.log("submitCreateArchive");

	if(form.elements["archive_name"].value.length == 0){
		raiseError("Archive name is not set");
		return;
	}
   
    if(form.elements["userfile[]"].files.length == 0){
		raiseError("No files are selected");
		return;
    }
    
	form.submit();
}