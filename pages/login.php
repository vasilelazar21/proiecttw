<html>
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/login.css">
    <script src="../JsScripts/Utils.js"></script>
  </head>
  <?php 
    require("../PhpScripts/UserManager.php");
    session_start();

    $error_message = "";

    if (isset($_SESSION['user_id'])) {
      if($_SESSION["role"] == "admin"){
        header("Location: admin.php");
      }else if($_SESSION["role"]  == "client"){
        header("Location: list_archive.php");
      }
    }else{
      $error_message = execute_login();
    }

  ?>
  <script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
  </script>
  <body>
    <header>
      <div class="header_flex">
          <h5>
            <?php echo "Guest"?>
          </h5>
      </div>
      <h1>
          ArchivR
      </h1>
    </header>
      
    <div id="error_message" class="error_panel hidden"></div>  

      <div class="main_container">
            <div class="page_title">
                <h3>Login</h3>
            </div>
          
            <div class="main_content">

                <form id="login_form" action="" method="post">
                    <img src="../images/ic_avatar.png" alt="Avatar" class="avatar">

                    <input type="text" placeholder="Enter Email" name="email" required>

                    <input type="password" placeholder="Enter Password" name="password" required>

                    <div type="submit" class="button" onclick="submitLogin(document.getElementById('login_form'));">Login</div>

                    <div class="psw">
                        Don't have an account? 
                        <a href="../pages/register.php">Register now!</a> 
                    </div>
                    
                    <div class="psw"><a href="forgot_password.php">Forgot password?</a></div>
                </form>  

            </div>
    </div>
      
        <footer>
        <h5>
            Archivar v1.0.0
            <a href="contact.php">Contact</a>
            <a href="info.php">Info</a>
        </h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>
  </body>
</html>