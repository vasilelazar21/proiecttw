<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/forgot_password.css">
    <script src="../JsScripts/Utils.js"></script>
</head>
<?php
include_once "../PhpScripts/forgot_password.php";
?>
<script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
</script>
    <body>
        <header>
            <div class="header_flex">
                <h5>Guest</h5>
            </div>
            <h1>ArchivR</h1>
        </header>
        <div id="error_message" class="error_panel hidden"></div>

        <div class="main_container">
            <div class="main_content">
                <form method = "POST" action = "">
                    <p>Forgot your password?</p>
                    <input type="text" placeholder="Username account" name="username">
                    <input type="text" placeholder="Email account" name="email">
                    <button class="button" name="submit">Submit</button>
                </form>
            </div>
        </div>
        
            <footer>
        <h5>
            Archivar v1.0.0
            <a href="contact.php">Contact</a>
            <a href="info.php">Info</a>
        </h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>

  </body>
</html>      