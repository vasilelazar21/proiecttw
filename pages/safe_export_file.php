<?php 
  session_start();
  require("../PhpScripts/StatsManager.php");

  if (isset($_SESSION['user_id'])) {

    if(isset($_POST["format"])){

      $download_path =  export_stats($_POST["format"]);
      echo "path : [" . $download_path . "]";
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename=' . basename($download_path));
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Pragma: public');
      header('Content-Length: ' . filesize($download_path));
      ob_clean();
      flush();
      readfile($download_path);
      $_POST = [];
    } else{
      header("Location: stats.php");
    }

  } else {
    header("Location: login.php");
  } 
?>