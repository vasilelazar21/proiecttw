<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">    <title>Archivar</title>
    <link rel="stylesheet" href="../style/info.css">
  </head>

  <body>

    <header>
      <div class="header_flex">
          <h5>Info</h5>
      </div>
      <h1>
          ArchivR
      </h1>
    </header>
      
    <div class="error_panel hidden">
       Error message
    </div>  

      <div class="main_container">
            <div class="page_title">
                <h3>ArchivR [B]</h3>
            </div>
          
            <div class="main_content">

                <h4>
                    Sa se conceapa o aplicatie Web capabila sa realizeze arhivarea ori compresarea unor resurse (fisiere) transferate de utilizator prin 'upload'. Minimal, se vor pune la dispozitie mijloace de generare a unei arhive de tip BZIP2, GZIP, TAR si ZIP. Utilizatorii autentificati cu rol de administrator vor putea stabili diversi parametri de conform specificand care este dimensiunea maxima permisa a unui fisier ce trebuie arhivat, marimea si conventia de denumire a arhivei rezultate (e.g., numele de utilizatorului urmat de data curenta sau de tipul MIME daca toate fisierele sunt de acelasi tip), numarul maxim de fisiere ce pot fi incluse intr-o arhiva etc. Arhiva creata va putea fi ulterior consultata (minimal, listarea listei fisierelor pe care le contine). De asemenea, sistemul va oferi diverse rapoarte -- in formatele CSV, HTML si XML -- privind ultimele arhive create. Bonus: oferirea de extensii software (i.e. module) implementand procesarea altor tipuri de arhive.
                </h4>
                
                <h4>
                    Persoane alocate: 2
                </h4>
            </div>
    </div>
      
    <footer>
        <h5>
            Archivar v1.0.0
            <a href="contact.php">Contact</a>
            <a href="info.php">Info</a>
        </h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>

  </body>
</html>