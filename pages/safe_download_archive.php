<?php 
  session_start();
  require_once("../PhpScripts/ArchiveManager.php");
  require_once("../PhpScripts/SettingsReader.php");

  if (isset($_SESSION['user_id'])) {

    if(isset($_POST["archive_id"]) && $_POST["archive_id"] > 0
        && isset($_POST["archive_type"])){ 

      $download_path =  get_archive_download_path($_POST["archive_id"]);
      $upload_path =  get_archive_upload_path($_POST["archive_id"]);
     
      delete_directory($download_path);  
      mkdir($download_path, 0777, true);

      $archive_path = $download_path;
      $archive_path = $archive_path . "/" . get_archive_name($_POST["archive_id"]) . "_" . get_archive_default_suffix($_SESSION["user_id"], $_SESSION["user_email"]);

      if($_POST["archive_type"] == 'zip'){
        $archive_path = $archive_path . "_zip.zip";
        zip_directory($upload_path,  $archive_path);
      }else if($_POST["archive_type"] == 'tar'){
        $archive_path = $archive_path . "_tar.tar";
        tar_directory($upload_path,  $archive_path);
      }else if($_POST["archive_type"] == 'gzip'){
        $archive_path = $archive_path . "_gzip.gz";
        gzip_directory($upload_path,  $archive_path);
      }else if($_POST["archive_type"] == 'bzip2'){
        $archive_path = $archive_path . "_bzip2.bz2";
        gzip_directory($upload_path,  $archive_path);
      }else{
        header("Location: list_archive.php");
        exit;
      }

      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename=' . basename($archive_path));
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Pragma: public');
      header('Content-Length: ' . filesize($archive_path));
      ob_clean();
      flush();
      readfile($archive_path);
      $_POST = [];
    } else{
      header("Location: list_archive.php");
    }

  } else {
    header("Location: login.php");
  } 
?>