<?php 
  session_start();
  require_once("../PhpScripts/ArchiveManager.php");

  if (isset($_SESSION['user_id'])) {
      if(isset($_POST["archive_id"]) && $_POST["archive_id"] > 0){
        delete_archive($_POST["archive_id"]);
        $_POST = [];
      }
      header("Location: list_archive.php");
  } else {
    header("Location: login.php");
  } 
?>
