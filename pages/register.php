<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/register.css">
    <script src="../JsScripts/Utils.js"></script>
  </head>

  <?php 
      require("../PhpScripts/UserManager.php");
      session_start();

      $error_message = "";

      if (isset($_SESSION['user_id'])) {
        header("Location: login.php");
      } else {
         $error_message = execute_register();
      } 
  ?>
  <script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
  </script>
  
  <body>
    <header>
      <div class="header_flex">
          <h5>Guest</h5>
      </div>
      <h1>
          ArchivR
      </h1>
    </header>
      
     <div id="error_message" class="error_panel hidden"> </div>

      <div class="main_container">
          
        <div class="page_title">
            <h3>Register</h3>
        </div>
          
        <div class="main_content">
           
            <form id="register_form" method="post" action="">
                <b>All fields are required!</b>
                <input type="text" placeholder="First name" name="first_name">
                <input type="text" placeholder="Last name" name="last_name">
                <input type="text" placeholder="Email Adress" name="email">         
                <input type="password" placeholder="Password" name="password">
                <input type="password" placeholder="Confirm password" name="confirm_password">
                <div class="button" onclick="submitRegister(document.getElementById('register_form'));">Register</div>
                
                <div class="psw">Already registered ?<a href="login.php"> Login </a></div>
                <div class="psw"><a href="forgot_password.php">Forgot password?</a></div>
                   
            </form> 
            
        </div>
      
      </div>
      
        <footer>
        <h5>
            Archivar v1.0.0
            <a href="contact.php">Contact</a>
            <a href="info.php">Info</a>
        </h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>

  </body>
</html>