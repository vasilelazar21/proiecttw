<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/user.css">
    <script src="../JsScripts/Utils.js"></script>
    <script src="../JsScripts/CreateArchiveHelper.js"></script>
  </head>
 <?php 
    require("../PhpScripts/UserManager.php");
    require("../PhpScripts/ArchiveManager.php");
    session_start();

    $error_message = "";
    if (isset($_SESSION['user_id'])) {

    } else {
      header("Location: login.php");
    } 
  ?>
  <script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
  </script>
  <body>
      
    <header>
      <h1>ArchivR</h1>
      <div class="header_flex">
          <h5 class="button" onclick="submitLogout()">Log out</h5>
          <h5><?php echo $_SESSION["user_email"]; ?></h5>
      </div>
    </header>
      
    <div id="error_message" class="error_panel hidden"></div>  

    <div class="main_container">

        <div class="main_content">
            <div class="horizontal_selector">
                <div class="button" onclick="window.location='create_archive.php'">New Archive</div>
                <div class="button selected" onclick="window.location='list_archive.php'">List archives</div>
                <div class="button" onclick="window.location='stats.php'">Stats</div>
            </div>
        </div>

        <div class="main_content">
                <div id="archive_list_container">
                     <div class="table">
                    <table>
                      <tr>
                        <th>Name</th>
                        <th>Files</th>
                        <th>Create datetime</th> 
                        <th>Dimension</th> 
                        <th>Options</th> 
                      </tr>
                      <?php get_archive_list();?>         
                    </table>
                    </div>
                </div>
        </div>
    </div>

    <form id="delete_archive_form" method="post" action="delete_archive.php">
        <input id="delete_archive_id" type="hidden" name="archive_id" value = "-1">
    </form> 

    <footer>
      <h5>Archivar v1.0.0</h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>

     <div id="popup" class="popup_layer hidden">
        <div class="popup_box">
            <h3>Are you sure ?</h3>
            <div class="buttons_holder">
                <div class="button" onclick="close_popup()">No</div>
                <div class="button" onclick="close_popup()">Yes</div>
            </div>
        </div>
         
         <script>
             function close_popup(){
                document.getElementById("popup").classList.add('hidden');
             }
         </script>
    </div>

    <div id="download_popup" class="popup_layer hidden">
        <div class="popup_box">
            <h3>Choose archive format</h3>
            <form id="download_archive_form" method="post" action="safe_download_archive.php">
                <input id="download_archive_id" type="hidden" name="archive_id" value = "-1">
                <input type="radio" name="archive_type" value="zip" checked>ZIP
                <input type="radio" name="archive_type" value="gzip">GZIP
                <input type="radio" name="archive_type" value="tar">TAR 
                <input type="radio" name="archive_type" value="bzip2">BZIP2
            </form> 
            <br>
            <div class="buttons_holder">
                <div class="button" onclick="closeDownloadPopup()">Cancel</div>
                <div class="button" onclick="submitDownloadArchive()">Download</div>
            </div>
        </div>
         
         <script>
             function close_popup(){
                document.getElementById("popup").classList.add('hidden');
             }
         </script>
    </div>
      
    <script>
        
        function prepareDeleteArchive(id){
            document.getElementById("delete_archive_id").value = id;
            document.getElementById("delete_archive_form").submit();
        }

        function prepareDownloadArchive(id){
            document.getElementById("download_archive_id").value = id;
            document.getElementById("download_popup").classList.remove("hidden");
        }

        function submitDownloadArchive(){
            closeDownloadPopup();
            document.getElementById("download_archive_form").submit();
        }

        function closeDownloadPopup(){
            document.getElementById("download_popup").classList.add("hidden");
        }

        function toggleArchiveFilesList(caller, id){
            var containerId = 'list_files_' + id;
            if(document.getElementById(containerId).classList.contains('hidden')){
                document.getElementById(containerId).classList.remove('hidden');
                caller.classList.add('rotate');
            }else{
                document.getElementById(containerId).classList.add('hidden');
                caller.classList.remove('rotate');
            }
        }
     
        function toggle_error(){
            if(document.getElementById("error_message").classList.contains('hidden')){
                document.getElementById("error_message").classList.remove('hidden');
            }else{
                document.getElementById("error_message").classList.add('hidden');
            } 
        }
        
        function open_popup(){
            document.getElementById("popup").classList.remove('hidden');
        }
        
    </script>
  
    </body>
</html>