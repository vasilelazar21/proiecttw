<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/admin.css">
    <script src="../JsScripts/Utils.js"></script>
  </head>
  <?php 
    require("../PhpScripts/UserManager.php");
    include_once "../PhpScripts/SettingsFunctions.php";
    include_once "../PhpScripts/Admin_userdManager.php"; 
    session_start();

    if (!isset($_SESSION['user_id'])) {
      header("Location: login.php");
    }
    
  ?>

  <body>
      
     <header>
      <div class="header_flex">
      <h5 class="button" onclick="submitLogout()">Log out</h5>
      <h5><?php echo $_SESSION["user_email"]; ?></h5>
      </div>
      <h1>
          ArchivR
      </h1>
    </header>
      
       <div class="error_panel hidden">Error Message</div>
        <div class="main_container">
            <div class="main_content">
                <div class="main_left">
                    <a href="admin.php">
                    <div class="button">
                        Settings
                    </div>
                    </a>
                    <div class="button selected">
                        Users details
                    </div>
                        </div>
                <div class="main_right">
                   
                   <!--########## Users ############-->
                <div class="table">
                <table>
                      <tr>
                        <th>Id</th>
                        <th>Fullname</th>
                        <th>Email</th> 
                        <th>Used Disk</th> 
                        <th>Send mail</th>
                        <th>Delete</th> 
                      </tr>
                      <?php get_users_list(); ?>
                    </table>
                    </div>
                    <!--########## Users ############-->
                </div>
            </div>

              <form id = "popup" class="popup_layer hidden" action="" method="POST">
              <div class="popup_box">
                <h3> Are you sure? </h3>
                  <div class= "buttons_holder">
                    <button type="submit" name="submit" onclick="close_popup()">Yes</button>
                    <button type="submit" name="submit" onclick="close_popup()">No</button>
                  </div>
              </div>
              </form>
         
              <script>      
        function open_popup(){
            document.getElementById("popup").classList.remove('hidden');
        }

         function close_popup(){
            document.getElementById("popup").classList.add('hidden');
        }
    </script>
    </div>
        </div>
          <footer>
        <h5>
            Archivar v1.0.0
            <a href="contact.html">Contact</a>
            <a href="info.html">Info</a>
        </h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>
     
    </body>
</html>