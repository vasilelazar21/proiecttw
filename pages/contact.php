<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">    <title>Archivar</title>
    <link rel="stylesheet" href="../style/contact.css">
  </head>

  <body>

    <header>
      <div class="header_flex">
          <h5>Contact</h5>
      </div>
      <h1>
          ArchivR
      </h1>
    </header>
      
    <div class="error_panel hidden">
        Mesaj de eroare
    </div>  

      <div class="main_container">
            <div class="page_title">
                <h3>Contact</h3>
            </div>
          
            <div class="main_content">

                <h3>
                   Please never contact us at 
                </h3>
                <h3>
                    <a href="mailto:traistarafael@gmail.com">traistarafael@gmail.com</a>
                </h3> 
                <h3>
                    or 
                </h3>
                <h3>
                    <a href="mailto:lazarvasile@gmail.com">lazarvasile@gmail.com</a>
                </h3> 
                <h3>
                    Thank you for understanding !
                </h3>
                
            </div>
    </div>
      
    <footer>
        <h5>
            Archivar v1.0.0
            <a href="contact.html">Contact</a>
            <a href="info.html">Info</a>
        </h5>
      
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>

  </body>
</html>