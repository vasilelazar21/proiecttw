<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/user.css">
    <script src="../JsScripts/Utils.js"></script>
    <script src="../JsScripts/CreateArchiveHelper.js"></script>
  </head>
 <?php 
    require("../PhpScripts/UserManager.php");
    require("../PhpScripts/ArchiveManager.php");
    require_once("../PhpScripts/SettingsReader.php");

    session_start();

    $error_message = "";

    if (isset($_SESSION['user_id'])) {
      if(isset($_FILES["userfile"])){
        $error_message = make_archive();
      }
    } else {
      header("Location: login.php");
    } 
  ?>
  <script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
  </script>
  <body>
      
    <header>
      <h1>ArchivR</h1>
      <div class="header_flex">
          <h5 class="button" onclick="submitLogout()">Log out</h5>
          <h5><?php  echo  $_SESSION["user_email"]; ?></h5>
      </div>
     
    </header>
      
    <div id="error_message" class="error_panel hidden"></div>  

    <div class="main_container">

        <div class="main_content">
            <div class="horizontal_selector">
                <div class="button selected" onclick="window.location='create_archive.php'">New Archive</div>
                <div class="button" onclick="window.location='list_archive.php'">List archives</div>
                <div class="button" onclick="window.location='stats.php'">Stats</div>
            </div>
        </div>

        <div class="main_content">
          <form id="create_archive_form" method="post" action="" enctype="multipart/form-data">
            <ul>
              <li>Max number of uploaded files : <?php echo get_max_upload_files_count(); ?></li>
              <li>Max file dimension : <?php echo get_max_upload_file_dimension() . 'kb'; ?></li>
            </ul>
              <input type="text" placeholder="Archive name" name="archive_name">
              <input id="0" name="userfile[]" type="file" multiple><br/>  
              <div class="button" onclick="submitCreateArchive(document.getElementById('create_archive_form'));">Make archive</div>
          </form> 
        </div>
    </div>

    <footer>
      <h5>Archivar v1.0.0</h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>

     <div id="popup" class="popup_layer hidden">
        <div class="popup_box">
            <h3>Are you sure ?</h3>
            <div class="buttons_holder">
                <div class="button" onclick="close_popup()">No</div>
                <div class="button" onclick="close_popup()">Yes</div>
            </div>
        </div>
         
         <script>

            function setArchvieType(button){
              document.getElementById("input_archive_type").value = button.id;
              button.classList.dd
            } 

            function close_popup(){
              document.getElementById("popup").classList.add('hidden');
            }

         </script>
    </div>
      
    </body>
</html>
