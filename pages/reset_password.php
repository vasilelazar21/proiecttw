<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/reset_password.css">
    <script src="../JsScripts/Utils.js"></script>
</head>
<?php
$error_message = null;
include_once "../PhpScripts/reset_password.php";

?>
<script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
</script>
<body>
        <header>
            <div class="header_flex">
                <h5>Guest</h5>
            </div>
            <h1>ArchivR</h1>
        </header>
        <div id="error_message" class="error_panel hidden"></div> 

        <div class="main_container">
            <div class="main_content">
                <form method = "POST" action = "">
                <p>Get new password</p>
                <input type="password" placeholder="New password" name="pass">
                <input type="password" placeholder="Confirm new password" name="reset_pass">
                <button class="button" name="submit">Reset password</button>
                </form>
            </div>
        </div>
        
           <footer>
        <h5>
            Archivar v1.0.0
            <a href="contact.php">Contact</a>
            <a href="info.php">Info</a>
        </h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>
        
  </body>
</html>      