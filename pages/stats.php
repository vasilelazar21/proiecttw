<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/user.css">
    <script src="../JsScripts/Utils.js"></script>
  </head>
 <?php 
    require("../PhpScripts/StatsController.php");
    session_start();

    $error_message = "";

    if (isset($_SESSION['user_id'])) {
        
    } else {
      header("Location: login.php");
    } 
  ?>
  <script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
  </script>
  <body>
      
    <header>
      <h1>ArchivR</h1>
      <div class="header_flex">
          <h5 class="button" onclick="submitLogout()">Log out</h5>
          <h5><?php echo $_SESSION["user_email"]; ?></h5>
      </div>
    </header>
      
    <div id="error_message" class="error_panel hidden"></div>  

    <div class="main_container">

        <div class="main_content">
            <div class="horizontal_selector">
                <div class="button" onclick="window.location='create_archive.php'">New Archive</div>
                <div class="button" onclick="window.location='list_archive.php'">List archives</div>
                <div class="button selected" onclick="window.location='stats.php'">Stats</div>
            </div>
        </div>

        <div class="main_content center_content">
            <h3>General info</h3>
            <?php echo get_general_stats();?>        
        </div>     
        
        <div class="main_content center_content">
     
                <h3>Export statistics</h3>
                
                <form id="create_archive_container" method="post" action="safe_export_file.php">
                    <h4>
                        <span>From:</span> 
                        <input type="date" name="start_datetime">
                    </h4>
                    
                    <h4>
                        <span>To:</span>
                        <input type="date" name="end_datetime">
                    </h4>
               
                    <h4>
                        <span>Format:</span>
                    </h4>
                    <h4>
                        <input type="radio" name="format" value="html" checked>HTML<br>
                        <input type="radio" name="format" value="csv">CSV<br>
                        <input type="radio" name="format" value="xml">XML<br>
                    </h4>
                    <input class="button" type="submit" value="Export">
            
                </form>
        </div>     

            <!--################ account settings ################-->
            <!-- <div id="account_settings_container" class="center_content hidden">
                <h4><span>Email:</span> testuser@gmail.com</h4>
                <h4><span>Username:</span> default_username</h4>
                <h4><span>Access:</span> Admin, Client</h4>
                <h4><span>Register datetime:</span> 15-jan-2019</h4>

                <form>
                    <input type="text" placeholder="Current password">
                    <input type="text" placeholder="New password">
                    <input type="text" placeholder="Confirm new password">
                    <div class="button">Change password</div>
                </form>
                
            </div> -->
                <!--################ account settings ################-->
       
    </div>

    <footer>
      <h5>Archivar v1.0.0</h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>

    </body>
</html>