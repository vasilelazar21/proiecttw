<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Archivar</title>
    <link rel="stylesheet" href="../style/admin.css">
    <script src="../JsScripts/Utils.js"></script>
  </head>
  <?php 
    include_once "../PhpScripts/SettingsManager.php";
    require("../PhpScripts/UserManager.php");
    session_start();

    if (isset($_SESSION['user_id'])) {
        if($_SESSION["role"]  == "client"){
          header("Location: list_archive.php");
        }
      }else{
        header("Location: login.php");
      }
    
  ?>
    <script>
    var lastPhpError = <?php  echo '"' . $error_message . '"'; ?>;
  </script>
  <body>
 

    <header>
      <h1>ArchivR</h1>
      <div class="header_flex">
          <h5 class="button" onclick="submitLogout()">Log out</h5>
          <h5><?php echo $_SESSION["user_email"]; ?></h5>
      </div>
    </header>
    <div id="error_message" class="error_panel hidden"></div> 
        
       <div class="error_panel hidden">Error Message</div>
      
        <div class="main_container">
            <div class="main_content">
                <div class="main_left">
                    <div class="button selected">
                        Settings
                    </div>
                    <a href="admin_userd.php">
                    <div class="button">
                        Users details
                    </div>
                    </a>
                </div>
                <div class="main_right">
                    <!-- ########## Settings ######## -->
                    <form action="" method="POST">
                    <p>Suffix rules:</p>
                    <ul>
                        <li>
                            What is between [  ] means value of, it can be one of the following:
                            <ol>
                                <li>
                                    <span>Username</span>
                                </li>
                                <li>
                                    <span>Email</span>
                                </li>
                                <li>
                                    <span>Datetime</span>
                                </li>
                                <li>
                                    <span>Random</span> //random number
                                </li>
                            
                            </ol>
                        </li>
                        <li>
                            Suffix cannot contains the following characters : ' '(space), $, \, /, .(dot), =
                        </li>
                        <li>
                            Eg:archive_[ email ]_[ yyyy ]_[ mm ]_[ dd ]
                        </li>
                    </ul>
                    <div class="set-name">
                      <p>Suffix:</p>  
                        <input style="text" placeholder="suffix" value="default_suffix_[random]" name="suffix">
                    </div>
                     <div class="set-dimention">
                        <p>
                        Set dimention:
                        </p>
                        
                            <div class="input-number"><input type="number" name="dimension"></div>
                            <div>
                                <select name = "type_size">
                                    <option value="Kb">Kb</option>
                                    <option value="Mb">Mb</option>
                                    <option value="Gb">Gb</option>
                                </select>
                            </div>
                        
                            
                    </div>
                    <div class="select-type">
                        <p>Select the types:</p>
                        <div class="rar">
                        <input type="checkbox" name="bzip2" class="checkrar">Bzip2
                        </div>
                        <div class="rar">
                        <input type="checkbox" name="gzip" class="checkrar">Gzip</div >
                        <div class="rar">
                        <input type="checkbox" name="tar" class="checkrar">Tar
                            </div>
                        <div class="rar">
                        <input type="checkbox" name="zip" class="checkrar">Zip
                        </div>
                    
                    </div>
                    <div class="select-number">
                        <p>Select number of files:</p>
                        <div class="custom-select">
                           <input type="number" min="1" max="200" name="number_of_files">
                            
                        </div>
                        
                        
                    </div>
                    <button id="submit" name="submit">Submit settings</button>

                </form>
                    <!-- ########## Settings ######## -->
                    
                </div>
            </div>
        </div>
          <footer>
        <h5>
            Archivar v1.0.0
        
        </h5>
      <h5>©Copyright 2019 - Lazar Vasile, Traista Rafael</h5>
    </footer>
    <script>      
        function open_popup(){
            document.getElementById("popup").classList.remove('hidden');
        }

         function close_popup(){
            document.getElementById("popup").classList.add('hidden');
        }
    </script>
     
    </body>
</html>