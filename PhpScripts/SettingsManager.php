<?php

include_once "../pages/admin.php";
include_once "SettingsFunctions.php";

$suffix = null;
$dimension = null;
$type_size = null;
$rar = null;
$tar = null;
$zip = null;
$number_of_files = null;

$old_suffix = getSuffix();
$old_dimension = getDimension();
$old_type_size = getTypeSize();
$old_number_of_files = getNumberOfFiles();

$error_message = null;
$ok = 1;
$ok_suffix = 0;

if(isset($_POST['submit'])) {
    
    if(!isset($_POST['bzip2']) && !isset($_POST['gzip']) && !isset($_POST['tar']) && !isset($_POST['zip']))
        $error_message = "You have to choose atleast one type of archive!";
    
    if(empty(trim($_POST['dimension']))){
        $error_message = "Please, insert the dimension!";
    }

    if(empty(trim($_POST['suffix']))){
        $error_message = "Please, insert the suffix!";
    }
    
    if(empty(trim($_POST['dimension']))){
        $error_message = "Please, insert number_of_files!";
    }

    if(suffixisValid($_POST['suffix']) == false) {
        $error_message = "Invalid suffix!";
        $ok_suffix=1;
    }

    if($error_message == null) {

    if(isset($_POST['suffix'])) {
        $suffix = $_POST['suffix'];
        if(suffixIsValid($suffix)) {
            
            
                modifySuffix($suffix);
            
        }
    }

    if(isset($_POST['dimension']) && !empty($_POST['dimension'])) {
        $dimension = $_POST['dimension'];

        if($dimension != $old_dimension) {
            modifyDimension($dimension);
        }
    }

    if(isset($_POST['type_size'])) {
        $type_size = $_POST['type_size'];

        if($type_size != $old_type_size) {
            modifyTypeSize($type_size);
        }
    }
    

    if(isset($_POST['bzip2'])) {
        modifyBzip2(1);
    }
    else {
        modifyBzip2(0);
    }
    if(isset($_POST['gzip'])) {
        modifyGzip(1);
    }
    else {
        modifyGzip(0);
    }
    
    if(isset($_POST['tar'])) {
        modifyTar(1);
    }
    else {
        modifyTar(0);
    }

    if(isset($_POST['zip'])) {
        modifyZip(1);
    }
    else {
        modifyZip(0);
    }

    $error_message = "Changes have been made successfully!";

}
else{
    if($ok_suffix == 0){
    $error_message = "Please fill in all fields!";
    }
    else {
        $error_message = "Invalid suffix!";
    }
}


}
?>