<?php

function get_archive_default_suffix($username, $email){

  require "dbConnect.php";
  $return_value = "_";

  $sql = "SELECT * FROM settings limit 1";
  $result = $conn->query($sql);
  
  if ($result->num_rows > 0) {
     $row = $result->fetch_assoc();
     $return_value = $row["Suffix"];

     $return_value = str_replace("[userid]", $username, $return_value);
     $return_value = str_replace("[email]", $email, $return_value);
     $return_value = str_replace("[random]", rand(10000,99999), $return_value);
     $return_value = str_replace("[datetime]", date('Y-m-d'), $return_value);

  } else {
     echo "Failed to read settings";
  }

  $conn->close();

  return $return_value;
}


function get_max_upload_files_count(){

  require "dbConnect.php";
  $return_value = 0;

  $sql = "SELECT * FROM settings limit 1";
  $result = $conn->query($sql);
  
  if ($result->num_rows > 0) {
     $row = $result->fetch_assoc();
     $return_value = $row["Number_files"];
  } else {
     echo "Failed to read settings";
  }

  $conn->close();

  return $return_value;
}


function get_max_upload_file_dimension(){

  require "dbConnect.php";
  $return_value = 0;

  $sql = "SELECT * FROM settings limit 1";
  $result = $conn->query($sql);
  
  if ($result->num_rows > 0) {
     $row = $result->fetch_assoc();
     $return_value = $row["Dimension"];
  } else {
     echo "Failed to read settings";
  }

  $conn->close();

  return $return_value;
}

?>

