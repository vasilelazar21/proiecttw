<?php
require("../PhpScripts/SettingsReader.php");

function register_archive_in_db(){

  if(isset($_FILES['userfile'])) {
    require "dbConnect.php";

    $sql = "INSERT INTO archive(user_id, create_datetime, name) VALUES(?, NOW(), ?)";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "ds", $_SESSION["user_id"], $_POST["archive_name"]);
        if(mysqli_stmt_execute($stmt)){
          $last_id = $conn->insert_id;
          mysqli_stmt_close($stmt);
          mysqli_close($conn);
          return $last_id;
        }else {
          echo mysqli_error($conn);
        }
        mysqli_stmt_close($stmt);
    }
    mysqli_close($conn);
  }

  return -1;
}

function get_user_path($user_id){
  return '../PersistentUser/user_' . $user_id;
}

function get_current_user_path(){
  return '../PersistentUser/user_' . $_SESSION["user_id"];
}

function get_archive_path($archive_id){
  return get_current_user_path() . '/archive_' . $archive_id;
}

function get_archive_upload_path($archive_id){
  return get_archive_path($archive_id) . '/' . 'upload' . '/';
}

function get_archive_download_path($archive_id){
  return get_archive_path($archive_id) . '/' . 'download' . '/';
}

function upload_files($archive_id){

  $total = count($_FILES['userfile']['name']);

  $upload_dir = get_archive_upload_path($archive_id);
  if (!file_exists($upload_dir)) {
    mkdir($upload_dir, 0777, true);
  }else{
    return 'File override error';
  }

  if (!file_exists($upload_dir)) {
    return 'failed to create user dir ' . $upload_dir . '<br>';
  }

  
  $success = 0;

  for( $i=0 ; $i < $total ; $i++ ) {

    $tmpFilePath = $_FILES['userfile']['tmp_name'][$i];
    if ($tmpFilePath != ""){

      $newFilePath = $upload_dir . '/' .  $_FILES['userfile']['name'][$i];

      if(move_uploaded_file($tmpFilePath, $newFilePath)) {
        $success++;
      }
    }
  }
  return "Uploaded " . $success . "/" . $total . " files. Archive id: " . $archive_id;
}

function make_archive(){

  $total = count($_FILES['userfile']['name']);
  $max_files_count = get_max_upload_files_count();
  $max_file_size =  get_max_upload_file_dimension();

  if($total > get_max_upload_files_count()){
    return "Exceeded max files count of : " . $max_files_count . '<br>';
  }

  for( $i=0 ; $i < $total ; $i++ ) {
    if($_FILES['userfile']['size'][$i] > 1024 * $max_file_size){
      return "Exceeded max file size of : " . $max_file_size . '<br>';
    }
  }

  $archive_id = register_archive_in_db();
  if($archive_id < 0){
    return "Failed to register archive in database";
  }
  return upload_files($archive_id);
}

function delete_directory($dir) {
  if (!file_exists($dir)) {
      return true;
  }

  if (!is_dir($dir)) {
      return unlink($dir);
  }

  foreach (scandir($dir) as $item) {
      if ($item == '.' || $item == '..') {
          continue;
      }

      if (!delete_directory($dir . DIRECTORY_SEPARATOR . $item)) {
          return false;
      }

  }

  return rmdir($dir);
}

function delete_directory_content($dir) {
  if (!file_exists($dir)) {
      return true;
  }

  if (!is_dir($dir)) {
      return false;
  }

  foreach (scandir($dir) as $item) {
      if ($item == '.' || $item == '..') {
          continue;
      }
      unlink($item);
  }
}

function delete_archive($archive_id)
{
    require "dbConnect.php";

    $sql = "DELETE FROM archive WHERE id = ?";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "d", $archive_id);
        if(mysqli_stmt_execute($stmt)){
          $file = get_archive_path($archive_id);
          echo "remove dir at : [" . $file . "]<br>";
          if(file_exists($file)){
            delete_directory($file);
          }
        }else {
          echo mysqli_error($conn);
        }
        mysqli_stmt_close($stmt);
    }
    mysqli_close($conn);
}

function get_archive_files_count($archive_id)
{
  $result = 0;  
  $archive_dir = get_archive_upload_path($archive_id);
  $files = scandir($archive_dir);
  foreach($files as $file){
    if($file == '.' || $file == '..'){

    }else{
      $result++;
    }
  }
  return $result;
}

function folder_size($dir)
{
  $size = 0;

   foreach (glob(rtrim($dir, '/') . '/*', GLOB_NOSORT) as $each) {
       $size += is_file($each) ? filesize($each) : folder_size($each);
   }
   
   return $size;
}

function get_archive_size($archive_id)
{
  return folder_size(get_archive_upload_path($archive_id));
}

function is_archive_valid($archive_id){
  if(file_exists(get_archive_path($archive_id)) 
    && file_exists(get_archive_upload_path($archive_id))){
    return true;
  }else{
    return false;
  }
}

function get_archive_name($archive_id){
  require "dbConnect.php";

  $return_value = "";

  $sql = "SELECT id, name, type, create_datetime FROM archive WHERE id = ?";

  if($stmt = mysqli_prepare($conn, $sql)){
      $stmt->bind_param("d", $archive_id);
      
      if($stmt->execute()){
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $return_value = $row["name"];                            
      } else{
        mysqli_stmt_close($stmt);
        echo "Oops! Failed to read archive name. Something went wrong. Please try again later.";
      }
  }else{
    echo "Read archive name. Failed to prepare statement";
  }     
  mysqli_close($conn);

  return $return_value;
}

function get_archive_list(){
  require "dbConnect.php";
  $sql = "SELECT id, name, type, create_datetime FROM archive WHERE user_id = ?";

  if($stmt = mysqli_prepare($conn, $sql)){
      $stmt->bind_param("d", $_SESSION["user_id"]);
      
      if($stmt->execute()){
          $result = $stmt->get_result();
          while($row = $result->fetch_assoc()) {
            if(is_archive_valid($row["id"])){
              $archive_dir = get_archive_upload_path($row["id"]);
              echo '
              <tr>
                  <td>' . $row["name"] . '</td>
                  <td>' . get_archive_files_count($row["id"]) . '</td>
                  <td>' . $row["create_datetime"] . '</td>
                  <td>' . number_format(get_archive_size($row["id"])/(1024 * 1024), 2) . 'mb</td>
                  <td>
                      <span>
                        <img alt="download" src="../images/ic_download.png"
                        onclick="prepareDownloadArchive(' . $row["id"] . ')">
                        <img alt="delete" src="../images/ic_delete.png" 
                          onclick="prepareDeleteArchive(' . $row["id"] . ')">
                        <img alt="details" src="../images/ic_details.png"
                        onclick="toggleArchiveFilesList(this, ' . $row["id"] . ')">
                      </span>
                  </td>
                </tr>
              ';

              echo '<tr class="hidden" id="list_files_' . $row["id"] . '"><td rowspan="1" colspan="5">';
              $files = scandir($archive_dir);
              foreach($files as $file){
                if($file == '.' || $file == '..'){

                }else{
                  echo '
                  <div class="file_input" style="margin:10px; width:auto;" >
                      <img alt="file" src="../images/ic_image.png">
                      <span class="separator"></span>
                      <div>' . number_format(filesize($archive_dir . '/' . $file)/1024, 2, '.', '') . 'kb</div>
                      <span class="separator"></span>
                      <div>' . $file . '</div>
                  </div>
                  ';
                }
              }
              echo '</td></tr>';
            }
          }                   
      } else{
        mysqli_stmt_close($stmt);
        echo "Oops! Something went wrong. Please try again later.";
      }
  }else{
    echo "Failed to prepare statement";
  }     
  mysqli_close($conn);  
}

function zip_directory($dir, $export_file){

  if (file_exists($export_file)) unlink($export_file);

  // Get real path for our folder
  $rootPath = realpath($dir);

  // Initialize archive object
  $zip = new ZipArchive();
  $zip->open($export_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

  // Create recursive directory iterator
  /** @var SplFileInfo[] $files */
  $files = new RecursiveIteratorIterator(
      new RecursiveDirectoryIterator($rootPath),
      RecursiveIteratorIterator::LEAVES_ONLY
  );

  foreach ($files as $name => $file)
  {
      // Skip directories (they would be added automatically)
      if (!$file->isDir())
      {
          // Get real and relative path for current file
          $filePath = $file->getRealPath();
          $relativePath = substr($filePath, strlen($rootPath) + 1);

          // Add current file to archive
          $zip->addFile($filePath, $relativePath);
      }
  }

  // Zip archive will be created only after closing object
  $zip->close();
}


function tar_directory($dir, $export_file){
  if (file_exists($export_file)) unlink($export_file);
  $phar = new PharData($export_file);
  $phar->buildFromDirectory($dir);
}

function gzip_directory($dir, $export_file){
  if (file_exists($export_file)) {
  
    echo "file exists<br>";
    unlink($export_file);
  }
  $phar = new PharData($export_file);
  $phar->buildFromDirectory($dir);
  $phar->compress(Phar::GZ);
}

function bzip2_directory($dir, $export_file){
  if (file_exists($export_file)) unlink($export_file);
  $phar = new PharData($export_file);
  $phar->buildFromDirectory($dir);
  $phar->compress(Phar::BZ2);
}
?>

