<?php
function hashInputPassword($string) {

    $hashed = hash("sha512", $string);
    return $hashed;
}

function execute_login(){

    if(isset($_POST["email"]) && isset($_POST["password"])){
        if(empty(trim($_POST["email"]))){
            return "Please enter an email.";
        } else{
            if(empty(trim($_POST["password"]))){
                return "Please enter a password.";
            }
            else
            {   
                require_once "dbConnect.php";
                $email = $_POST['email'];
                $password = $_POST['password'];
                $password = hashInputPassword($password);
                $sql = "SELECT id, role FROM user WHERE email = ? AND password = ?";

                if($stmt = mysqli_prepare($conn, $sql)){
                    $stmt->bind_param("ss", $email, $password);
                    
                    if($stmt->execute()){
                        $stmt_result = $stmt->get_result();
                        if($stmt_result->num_rows === 1){
                            $row = $stmt_result->fetch_assoc();
                            $rol = $row['role'];
                            $user_id = $row['id'];
                            if($rol == 'client'){
                                $_SESSION["user_id"] = $user_id;
                                $_SESSION["user_email"] = $email;
                                $_SESSION["role"] = $rol;
                                mysqli_stmt_close($stmt);
                                mysqli_close($conn); 
                                header("Location: list_archive.php");
                            } else if($rol== 'admin') {
                                $_SESSION["user_id"] = $user_id;
                                $_SESSION["user_email"] = $email;
                                $_SESSION["role"] = $rol;
                                mysqli_stmt_close($stmt);
                                mysqli_close($conn);
                                header("Location: admin.php");
                             }
                        }else{
                            mysqli_stmt_close($stmt);
                            mysqli_close($conn); 
                            return "Invalid email or password! Please try again!";
                        }                    
                    } else{
                        mysqli_stmt_close($stmt);
                        mysqli_close($conn); 
                        return "Oops! Something went wrong. Please try again later.";
                    }
                }else{
                    mysqli_stmt_close($stmt);
                    mysqli_close($conn); 
                    return "Failed to prepare statement";
                }  
                mysqli_close($conn);     
            }
        }          
    }else{
        return "";
    }
}

function execute_register(){
    $ok=0;
    $emails_list = array("@yahoo.com", "@gmail.com", "@info.uaic.ro");
    if(isset($_POST['email'])) {
        require_once "dbConnect.php";
        if(empty(trim($_POST['first_name']))){
            return "Please insert your first name!";
            $ok=1;
        }

        if(empty(trim($_POST['last_name']))){
            return "Please insert your last name!";
            $ok=1;
        }
        
        if(empty(trim($_POST['email']))){
            return "Please insert your email!";
            $ok=1;
        }
        
        if(empty(trim($_POST['password']))){
            return "Please insert your password!!";
            $ok=1;
        }

        if(empty(trim($_POST['confirm_password']))){
            return "Please insert your confirmed password!";
            $ok=1;
        }

        if($ok==0){
            
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $confirm_pass = $_POST['confirm_password'];
            $password = hashInputPassword($password);
            $confirm_pass = hashinputPassword($confirm_pass);

            $correct_email = 0;
            $exist_email = 0;
            for($i = 0; $i < sizeof($emails_list) && $correct_email == 0; $i++ ) {
                if(strpos($email, $emails_list[$i]) !== false)
                    $correct_email = 1;
            }

            if($correct_email == 1){
                $sql = "SELECT id FROM user WHERE email = ?";
                if($stmt = mysqli_prepare($conn, $sql)) {
                    mysqli_stmt_bind_param($stmt, "s", $email);

                    if(mysqli_stmt_execute($stmt)){
                        mysqli_stmt_store_result($stmt);

                        if(mysqli_stmt_num_rows($stmt) == 1){
                            return "This email already exists!";
                            $exist_email = 1;
                        }
                    }
                    if($exist_email ==0) {
                        
                        if($password != $confirm_pass)
                        return "Password and confirmation don't match!";
                        else{
                            $token = "1234567890qwertyuiopasdfghjklzxcvbnm";
                            $token = str_shuffle($token);
                            $token = substr($token, 0, 10);
                            $sql1 = "INSERT INTO user(email, first_name, last_name, password,role,token) VALUES(?,?,?,?,'client',?)";
                            if($stmt1 = mysqli_prepare($conn, $sql1)) {
                                mysqli_stmt_bind_param($stmt1, "sssss", $email, $first_name, $last_name, $password, $token );

                                if(mysqli_stmt_execute($stmt1)){

                                    mysqli_stmt_store_result($stmt1);

                                } else {
                                    return mysqli_error($conn);
                                }
                                mysqli_stmt_close($stmt1);
                                header("Location: login.php");
                                return ;
                            }
                            else return mysqli_error($conn);
                        
                        }
                    }
                
                }
                mysqli_stmt_close($stmt);
            }
            else{
                return "Format of email is wrong! Please try again!";
            }
        }
        mysqli_close($conn);
    }else{
        return "";
    }
}

function execute_logout(){
    $_SESSION = [];
    header("Location: login.php");
}

?>

