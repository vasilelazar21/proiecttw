<?php

function get_html_stats($user_id, $start_date, $end_date){
  include "dbConnect.php";

  $return_value = '
  <!DOCTYPE html>
  <html lang="en">
  <head>
  <title>Ahivr exported</title>
  <style>
      table, th, td {
      border: 1px solid black;
      border-collapse: collapse;
      }

      th{
      padding:10px;
      }

      table{
          width:100%;
          border:none;
      }

      td{
          text-align: center;
      }
    </style>
  </head>
  <body>
  <table>
  <tr>
    <th>Name</th>
    <th>Files</th>
    <th>Create datetime</th> 
    <th>Dimension</th> 
  </tr>';

  $start_date = $start_date . ' 00:00:00';
  $end_date = $end_date . ' 00:00:00';

  $sql = 'SELECT id, name, type, create_datetime FROM archive WHERE user_id = ?
   and (create_datetime > "' . $start_date . '" and create_datetime < "' . $end_date . '")';

  if($stmt = mysqli_prepare($conn, $sql)){
  
      $stmt->bind_param("d", $user_id);
      
      if($stmt->execute()){
          $result = $stmt->get_result();
          while($row = $result->fetch_assoc()) {

            if(is_archive_valid($row["id"])){
              $archive_dir = get_archive_upload_path($row["id"]);
              $return_value = $return_value . '
              <tr>
                  <td>' . $row["name"] . '</td>
                  <td>' . get_archive_files_count($row["id"]) . '</td>
                  <td>' . $row["create_datetime"] . '</td>
                  <td>' . get_archive_size($row["id"]) . 'mb</td>
                </tr>
              ';

              $return_value = $return_value .  '<tr><td rowspan="1" colspan="5">';
              $files = scandir($archive_dir);
              foreach($files as $file){
                if($file == '.' || $file == '..'){

                }else{
                  $return_value = $return_value .  '
                  <div style="margin:10px; width:auto;" >
                      <span>' . number_format(filesize($archive_dir . '/' . $file)/1024, 2, '.', '') . '</span>
                      <span> | </span>
                      <span>' . $file . '</span>
                  </div>
                  ';
                }
              }
              $return_value = $return_value .  '</td></tr>';
            }
          }                   
      } else{
        mysqli_stmt_close($stmt);
        $return_value = $return_value .  "Oops! Something went wrong. Please try again later.";
      }
  }else{
    $return_value = $return_value .  "Failed to prepare statement";
  }     
  mysqli_close($conn); 
  
  $return_value = $return_value . "</table></body></html>";
  return $return_value;
}

function get_xml_stats($user_id, $start_date, $end_date){
  include "dbConnect.php";

  $return_value = '';

  $domtree = new DOMDocument('1.0', 'UTF-8');
  $domtree->formatOutput = true;

  $xml = $domtree->createElement("xml");
  $xml = $domtree->appendChild($xml);

  $start_date = $start_date . ' 00:00:00';
  $end_date = $end_date . ' 00:00:00';

  $sql = 'SELECT id, name, type, create_datetime FROM archive WHERE user_id = ?
   and (create_datetime > "' . $start_date . '" and create_datetime < "' . $end_date . '")';

  if($stmt = mysqli_prepare($conn, $sql)){
      $stmt->bind_param("d", $user_id);
      
      if($stmt->execute()){
          $result = $stmt->get_result();
          while($row = $result->fetch_assoc()) {
            if(is_archive_valid($row["id"])){
              $archive_dir = get_archive_upload_path($row["id"]);

              $new_archive = $domtree->createElement("archive");
              $new_archive = $xml->appendChild($new_archive);

              $new_archive->setAttribute('name', $row["name"]);
              $new_archive->setAttribute('files', get_archive_files_count($row["id"]));
              $new_archive->setAttribute('create_datetime', $row["create_datetime"]);
              $new_archive->setAttribute('size', get_archive_size($row["id"]) . 'mb');

              $files = scandir($archive_dir);
              foreach($files as $file){
                if($file == '.' || $file == '..'){

                }else{
                  $new_file = $domtree->createElement("file");
                  $new_file = $new_archive->appendChild($new_file);
                  $new_file->setAttribute('size', number_format(filesize($archive_dir . '/' . $file)/1024, 2, '.', ''));
                  $new_file->setAttribute('name',  $file);
                }
              }
            }
          }                   
      } else{
        mysqli_stmt_close($stmt);
        $return_value = $return_value .  "Oops! Something went wrong. Please try again later.";
      }
  }else{
    $return_value = $return_value .  "Failed to prepare statement";
  }     
  mysqli_close($conn); 
  
  //$return_value = $return_value . "</table></body></html>";
  
  $return_value = $domtree->saveXML();

  return $return_value;
}


function get_csv_stats($user_i, $start_date, $end_dated){
  include "dbConnect.php";

  $return_value = '';

  $start_date = $start_date . ' 00:00:00';
  $end_date = $end_date . ' 00:00:00';

  $sql = 'SELECT id, name, type, create_datetime FROM archive WHERE user_id = ?
   and (create_datetime > "' . $start_date . '" and create_datetime < "' . $end_date . '")';

  if($stmt = mysqli_prepare($conn, $sql)){
      $stmt->bind_param("d", $user_id);
      
      if($stmt->execute()){
          $result = $stmt->get_result();
          while($row = $result->fetch_assoc()) {
            if(is_archive_valid($row["id"])){
              $archive_dir = get_archive_upload_path($row["id"]);

              $return_value = $return_value . 'archive' . ',';
              $return_value = $return_value . $row["name"] . ',';
              $return_value = $return_value . get_archive_files_count($row["id"]) . ',';
              $return_value = $return_value . $row["create_datetime"] . ',';
              $return_value = $return_value . get_archive_size($row["id"]) . 'mb' . ',';
              $return_value = $return_value . "\n" . ',';
              $files = scandir($archive_dir);
              foreach($files as $file){
                if($file == '.' || $file == '..'){

                }else{
                  $return_value = $return_value . 'file' . ',';
                  $return_value = $return_value . number_format(filesize($archive_dir . '/' . $file)/1024, 2, '.', '') . ',';
                  $return_value = $return_value .  $file . ',';
                  $return_value = $return_value . "\n" . ',';
                }
              }
            }
            $return_value = $return_value . "\n" . ',';
          }                   
      } else{
        mysqli_stmt_close($stmt);
        $return_value = $return_value .  "Oops! Something went wrong. Please try again later.";
      }
  }else{
    $return_value = $return_value .  "Failed to prepare statement";
  }     
  mysqli_close($conn); 
  
  return $return_value;
}

function prepare_pdf_stats($export_path, $user_id){
  include('../xml2pdf/fpdf.php');

  $pdf = new FPDF();
  $pdf->AddPage();
  $pdf->SetFont('Arial','B',16);
  $pdf->Cell(40,10,'Hello World!');

  $pdf->Output('F', '$export_path', true);
}

?>

