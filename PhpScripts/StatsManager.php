<?php
require("ArchiveManager.php");
require("Exporters.php");

function get_archives_total(){
  
  $result = 0;
  include "dbConnect.php";

  $sql = "SELECT count(id) as rows_count FROM archive WHERE user_id = ?";

  if($stmt = mysqli_prepare($conn, $sql)){
    $stmt->bind_param("d", $_SESSION["user_id"]);    
    if($stmt->execute()){
      $sql_result = $stmt->get_result();
      $result = $sql_result->fetch_assoc()["rows_count"];
    }
    mysqli_stmt_close($stmt);
  }  
    
  mysqli_close($conn);  
  return $result;
}

function get_files_total(){
  $result = 0;
  include "dbConnect.php";
  
  $sql = "SELECT id FROM archive WHERE user_id = ?";

  if($stmt = mysqli_prepare($conn, $sql)){
    $stmt->bind_param("d", $_SESSION["user_id"]);
    if($stmt->execute()){
      $sql_result = $stmt->get_result();
      while($row = $sql_result->fetch_assoc()) {
        if(is_archive_valid($row["id"])){
          $result = $result + get_archive_files_count($row["id"]);
        }
      }    
    }
    mysqli_stmt_close($stmt);
  }

  mysqli_close($conn);  
  return $result;
}

function get_size_total(){
  $result = 0;
  include "dbConnect.php";
  $sql = "SELECT id FROM archive WHERE user_id = ?";

  if($stmt = mysqli_prepare($conn, $sql)){
    $stmt->bind_param("d", $_SESSION["user_id"]);
    if($stmt->execute()){
      $sql_result = $stmt->get_result();
      while($row = $sql_result->fetch_assoc()) {
        if(is_archive_valid($row["id"])){
          $result = $result + get_archive_size($row["id"]);
        }
      }    
    }
    mysqli_stmt_close($stmt);
  }

  mysqli_close($conn);  
  return $result;
}



function get_export_dir($user_id){
  return '../PersistentUser/user_' . $_SESSION["user_id"] . '/export';
}

function export_stats($format){

  $export_path = get_export_dir($_SESSION["user_id"]);
    
  delete_directory($export_path);
  mkdir($export_path, 0777, true);

  $export_data = '';
  $export_path = $export_path . "/export_" . $format . "." . $format;

  if($format == "html"){
    $export_data = get_html_stats($_SESSION["user_id"], $_POST["start_datetime"], $_POST["end_datetime"]);  
  }else if($format == "xml"){
    $export_data =  get_xml_stats($_SESSION["user_id"], $_POST["start_datetime"], $_POST["end_datetime"]);
  }else if($format == "csv"){
    $export_data =  get_csv_stats($_SESSION["user_id"], $_POST["start_datetime"], $_POST["end_datetime"]);
  }

  file_put_contents($export_path, $export_data);

  return  $export_path;
}
?>

