<?php

$ok=0;
$emails_list = array("@yahoo.com", "@gmail.com", "@info.uaic.ro");
if(isset($_POST['register'])) {
	require_once "dbConnect.php";
	if(empty(trim($_POST['first_name']))){
		echo "Please insert your first name!";
		$ok=1;
	}

	if(empty(trim($_POST['last_name']))){
		echo "Please insert your last name!";
		$ok=1;
	}
	
	if(empty(trim($_POST['email']))){
		echo "Please insert your email!";
		$ok=1;
	}
	
	if(empty(trim($_POST['password']))){
		echo "Please insert your first name!";
		$ok=1;
	}

	if(empty(trim($_POST['confirm_pass']))){
		echo "Please insert your confirmed password!";
		$ok=1;
	}

	if($ok==0){
		
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$confirm_pass = $_POST['confirm_pass'];

		$correct_email = 0;
		$exist_email = 0;
		for($i = 0; $i < sizeof($emails_list) && $correct_email == 0; $i++ ) {
			if(strpos($email, $emails_list[$i]) !== false)
				$correct_email = 1;
		}

		if($correct_email == 1){
			$sql = "SELECT id FROM user WHERE email = ?";
			if($stmt = mysqli_prepare($conn, $sql)) {
				mysqli_stmt_bind_param($stmt, "s", $email);

				if(mysqli_stmt_execute($stmt)){
					mysqli_stmt_store_result($stmt);

				if(mysqli_stmt_num_rows($stmt) == 1){
					echo "This email already exists!";
					$exist_email = 1;
				}
			}
				if($exist_email ==0) {
					
					if($password != $confirm_pass)
						echo "Passwords do not match!";
					else
					{
						$sql1 = "INSERT INTO user(email, first_name, last_name, password,role) VALUES(?,?,?,?,'client')";
						if($stmt1 = mysqli_prepare($conn, $sql1)) {
							mysqli_stmt_bind_param($stmt1, "ssss", $email, $first_name, $last_name, $password );

							if(mysqli_stmt_execute($stmt1)){

							mysqli_stmt_store_result($stmt1);

						}
						else
							echo mysqli_error($conn);
						mysqli_stmt_close($stmt1);
						echo "You have been registered!";
					
					}
					else
						echo mysqli_error($conn);
					
				}
			}
			


		}
		mysqli_stmt_close($stmt);

	}
	else{
		echo "Format of email is wrong! Please try again!";
	}
}
	mysqli_close($conn);
}

?>