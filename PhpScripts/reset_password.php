<?php
include "UserManager.php";
require_once "dbConnect.php";
$error_message = null;


if(isset($_GET['token'])){
$token= $_GET['token'];
}


if(isset($_POST['submit'])) {
    $sql = "SELECT token, id from user where token = ?";
    if($stmt = $conn->prepare($sql)){
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        if($row){
    $id = $row['id'];
    $pass = $_POST['pass'];
    $reset_pass = $_POST['reset_pass'];
    

    if($pass !== $reset_pass){
    $error_message = "Password and confirmation don't match!";
    }
    if($error_message == null && (strlen($pass)<6 || strlen($pass)>24)){
        $error_message = "Password must be between 6 and 24 characters!";
    }
    $pass = hashInputPassword($pass);
    $reset_pass = hashInputPassword($reset_pass);
    if($error_message == null) {
        $token2 = "1234567890qwertyuiopasdfghjklzxcvbnm";
        $token2 = str_shuffle($token2);
        $token2 = substr($token2, 0, 10);
        $sql = "UPDATE user set password = ?, token = ? where id = ? ";
        if($stmt = $conn->prepare($sql)){
        $stmt->bind_param("ssd", $pass, $token2, $id);
        $stmt->execute();
        $result = $stmt->store_result();

        header ("Location: ../Pages/login.php");
        }
        
        
    }
}
else {
    $error_message = "Token has expired!";
}
}
}



?>