<?php
require("StatsManager.php");

function get_general_stats(){
  $html = '';
  $html = $html . '<h4><span>Total archives:</span>' . get_archives_total() . '</h4>';
  $html = $html . '<h4><span>Total files:</span>' . get_files_total() . '</h4>';
  $html = $html . '<h4><span>Total files size:</span>' . number_format(folder_size(get_user_path($_SESSION["user_id"]))/(1024 * 1024), 2) . ' mb</h4>';

  return $html;
}
?>

