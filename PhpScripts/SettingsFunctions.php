<?php
require_once "dbConnect.php";
require_once "archiveManager.php";


function getSuffix() {
    global $conn;
    $sql = "SELECT suffix FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}

function getDimension() {
    global $conn;
    $sql = "SELECT dimension FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}

function getTypeSize() {
    global $conn;
    $sql = "SELECT type_size FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}

function getBzip2() {
    global $conn;
    $sql = "SELECT bzip2 FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}

function getGzip() {
    global $conn;
    $sql = "SELECT Gzip FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}

function getTar() {
    global $conn;
    $sql = "SELECT tar FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}

function getZip() {
    global $conn;
    $sql = "SELECT zip FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}   

function getNumberOfFiles() {
    global $conn;
    $sql = "SELECT number_files FROM settings where id = 1";
    $result = mysqli_query($conn, $sql);
    
    while($row = mysqli_fetch_row($result)) {
        return $row[0];
    }
}

function modifySuffix($new_suffix) {
    global $conn;
    $sql = "UPDATE settings set suffix = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "s", $new_suffix);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function modifyDimension($new_dimension) {
    global $conn;
    $sql = "UPDATE settings set dimension = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "d", $new_dimension);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function modifyTypeSize($new_type_size) {
    global $conn;
    $sql = "UPDATE settings set type_size = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "s", $new_type_size);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function modifyBzip2($new_Bzip2) {
    global $conn;
    $sql = "UPDATE settings set bzip2 = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $new_Bzip2);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function modifyGzip($new_Gzip) {
    global $conn;
    $sql = "UPDATE settings set gzip = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $new_Gzip);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}


function modifyTar($new_tar) {
    global $conn;
    $sql = "UPDATE settings set tar = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $new_tar);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function modifyZip($new_zip) {
    global $conn;
    $sql = "UPDATE settings set zip = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $new_zip);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function modifyNumberOfFiles($new_number_of_files) {
    global $conn;
    $sql = "UPDATE settings set number_files = ? where id = 1";
    if($stmt = mysqli_prepare($conn, $sql)) {
        mysqli_stmt_bind_param($stmt, "i", $new_number_of_files);

        if(mysqli_stmt_execute($stmt)) {
            mysqli_stmt_store_result($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function suffixIsValid($suffix) {
    
    
    if (strpos(strtolower($suffix), '[random]') == false && strpos(strtolower($suffix), '[username]') == false && strpos(strtolower($suffix), '[datetime]') == false && strpos(strtolower($suffix), '[email]') == false){ 
        return false;
    }



    if (strpos($suffix, ' ') !== false)
        return false;

    if (strpos($suffix, '\'') !== false)
        return false;
    
    if (strpos($suffix, '$') !== false)
        return false;
    
    if (strpos($suffix, '/') !== false)
        return false;
        
    if (strpos($suffix, '\\') !== false)
        return false;
    
    if (strpos($suffix, '.') !== false)
        return false;
    
    if (strpos($suffix, '=') !== false)
        return false;

    return true;

    
}

function extractNameUser($id) {
    global $conn;
    $sql = "SELECT first_name from user where id = ?";
    if($stmt = $conn->prepare($sql)) {
        $stmt->bind_param("d", $id);
        $stmt->execute();

        $result = $stmt->get_result();

        while($row = $result->fetch_assoc()) {
            $name = $row['first_name'];
        }

    }
    
    return $name;
}

//este corect sa aiba 0 mb, userul 3 nu a incarcat inca nimic, sau nu e corect :)
function get_users_list() {
    global $conn;
    require_once "ArchiveManager.php";

    $sql = "SELECT * from user where role = 'client'";

    $print = "";

    if($result = $conn->query($sql)) {
        while($row = $result->fetch_assoc()) {

            $print = $print .
            '<tr>
            <td>' . $row['id'] .'</td>
            <td>' . $row['first_name'] . ' ' .  $row['last_name'] . '</td>
            <td>' . $row['email']  . '</td>
            <td>' . number_format(folder_size(get_user_path($row['id']))/(1024 * 1024), 2) . ' Mb</td>
            <td>
                <button>
                    <a href="mailto:'. $row['email'] .'?Subject=Archivr">
                        <img src="../images/mail.png">
                    </a>
                </button>
            </td>
            <td> 
            <form action="" method="POST">
                <input type="text" name="username" value="'.$row['email'].'" hidden>
                <button type="submit" name="submit" value="delete">
                    <img src="../images/ic2_delete.png">
                </button>
            </form> 
            </td>
          </tr>';  
           
        }
    }

    echo $print;
}


?>